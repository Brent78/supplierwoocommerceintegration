package za.co.supplier.woocommerce.integration.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.supplier.woocommerce.integration.Entities.Role;
import za.co.supplier.woocommerce.integration.Entities.UserSWI;
import za.co.supplier.woocommerce.integration.Repository.RoleRepository;
import za.co.supplier.woocommerce.integration.Repository.UserSWIRepository;

import java.util.Collections;
import java.util.HashSet;

@Service("userSWIService")
public class UserSWISerive {

    private UserSWIRepository userSWIRepository;
    private RoleRepository roleRepository;
   // private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserSWISerive(UserSWIRepository userSWIRepository, RoleRepository roleRepository) {
        this.userSWIRepository = userSWIRepository;
        this.roleRepository = roleRepository;
        //this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public UserSWI findUserByEmail(String email) {
        return userSWIRepository.findByEmail(email);
    }

    public void saveUser(UserSWI user) {
        //user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        System.out.println("Pass: " + user.getPassword());
        Role userRole = roleRepository.findByRole("ADMIN");

        user.setRoles(new HashSet<>(Collections.singletonList(userRole)));

        //userSWIRepository.save(user);
    }
}
