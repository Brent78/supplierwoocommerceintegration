package za.co.supplier.woocommerce.integration.Configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@Configuration
public class HttpSessionConfig {

    @Bean
    public HttpSessionListener httpSessionListener() {
        return new HttpSessionListener() {
            @Override
            public void sessionCreated(HttpSessionEvent se) {
                System.out.println("Session Created with session id: " + se.getSession().getId());
                System.out.println("==== Session is created ====");
                //Works per second.
                se.getSession().setMaxInactiveInterval(2*60*60);
                System.out.println("CreateTime: " + se.getSession().getCreationTime());
                System.out.println("LastAccessedTeme: " + se.getSession().getLastAccessedTime());
            }

            @Override
            public void sessionDestroyed(HttpSessionEvent se) {
                System.out.println("==== Session is destroyed ====");
                System.out.println("Session Destroyed, Session id:" + se.getSession().getId());
            }
        };
    }

    @Bean                   // bean for http session attribute listener
    public HttpSessionAttributeListener httpSessionAttributeListener() {
        return new HttpSessionAttributeListener() {
            @Override
            public void attributeAdded(HttpSessionBindingEvent se) {            // This method will be automatically called when session attribute added
                String sessionId = se.getSession().getId();
                String attributeName = se.getName();
                Object attributeValue = se.getValue();
                System.out.println("Attribute added : " + attributeName + " : " + attributeValue);
            }
            @Override
            public void attributeRemoved(HttpSessionBindingEvent se) {      // This method will be automatically called when session attribute removed
                String attributeName = se.getName();
                Object attributeValue = se.getValue();
                System.out.println("Attribute removed : " + attributeName + " : " + attributeValue);
            }
            @Override
            public void attributeReplaced(HttpSessionBindingEvent se) {     // This method will be automatically called when session attribute replace
                String attributeName = se.getName();
                Object attributeValue = se.getValue();
                System.out.println("Attribute replaced : " + attributeName + " : " + attributeValue);
            }
        };
    }
}
