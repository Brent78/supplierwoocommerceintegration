package za.co.supplier.woocommerce.integration.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.supplier.woocommerce.integration.Entities.Profile;
import za.co.supplier.woocommerce.integration.Repository.ProfileRepository;

import java.util.Optional;

@Service
public class ProfileService extends AbstractService<Profile> {

    private ProfileRepository profileRepository;

    @Autowired
    public ProfileService(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    public ProfileRepository profileRepository() {
        return profileRepository;
    }

    public Profile findByEmail(String email) {

        if (email == null) {
            return error("Please provide a email!");
        }

        Profile profile = profileRepository.findByEmail(email);

        return successOrError("Profile was successfully get.", "There was some error!", profile);
    }
}
