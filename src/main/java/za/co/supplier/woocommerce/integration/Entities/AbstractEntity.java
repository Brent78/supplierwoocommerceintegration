package za.co.supplier.woocommerce.integration.Entities;

import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Optional;

@MappedSuperclass
@Data
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotFound(action = NotFoundAction.IGNORE)
    private Long id;

    @Transient
    private LoggerUtil loggerUtil;

    public Optional<Long> getIdOpt() {
        return Optional.ofNullable(id);
    }

    public Optional<LoggerUtil> getLoggerUtilOpt() {
        return Optional.ofNullable(loggerUtil);
    }
}
