package za.co.supplier.woocommerce.integration.Entities.Enums;

public enum LoggerUtilType {

    SUCCESS, INFO, WARNING, ERROR
}
