package za.co.supplier.woocommerce.integration.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import za.co.supplier.woocommerce.integration.Entities.UserSWI;

@Repository("userSWIRepository")
public interface UserSWIRepository extends JpaRepository<UserSWI, Long> {

    UserSWI findByEmail(String emial);
}
