package za.co.supplier.woocommerce.integration.Repository;

import org.springframework.stereotype.Repository;
import za.co.supplier.woocommerce.integration.Entities.Profile;

@Repository("profileRepository")
public interface ProfileRepository extends AbstractRepository<Profile> {

    Profile findByEmail(String email);
}
