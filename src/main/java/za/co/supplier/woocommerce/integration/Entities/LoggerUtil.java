package za.co.supplier.woocommerce.integration.Entities;

import za.co.supplier.woocommerce.integration.Entities.Enums.LoggerUtilType;

public class LoggerUtil {

    private String title;
    private String message;
    private LoggerUtilType loggerUtilType;

    private static final LoggerUtilType SUCCESS = LoggerUtilType.SUCCESS;
    private static final LoggerUtilType ERROR = LoggerUtilType.ERROR;
    private static final LoggerUtilType WARNING = LoggerUtilType.WARNING;
    private static final LoggerUtilType INFO = LoggerUtilType.INFO;

    public LoggerUtil() {
    }

    public LoggerUtil(String title, String message, LoggerUtilType loggerUtilType) {
        this.title = title;
        this.message = message;
        this.loggerUtilType = loggerUtilType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoggerUtilType getLoggerUtilType() {
        return loggerUtilType;
    }

    public void setLoggerUtilType(LoggerUtilType loggerUtilType) {
        this.loggerUtilType = loggerUtilType;
    }

    public static LoggerUtil success() {
        return new LoggerUtil(SUCCESS.toString(), "Process was successful?", SUCCESS);
    }

    public static LoggerUtil success(String message) {
        return new LoggerUtil(SUCCESS.toString(), message, SUCCESS);
    }

    public static LoggerUtil success(String title, String message) {
        return new LoggerUtil(title, message, SUCCESS);
    }

    public static LoggerUtil error() { return new LoggerUtil(ERROR.toString(), "There was some internal error!", ERROR); }

    public static  LoggerUtil error(String message) {
        return new LoggerUtil(ERROR.toString(), message, ERROR);
    }

    public static LoggerUtil error(String title, String message) {
        return new LoggerUtil(title, message, ERROR);
    }

    public static LoggerUtil warning(String title, String message) {
        return new LoggerUtil(title, message, WARNING);
    }

    public static LoggerUtil info(String title, String message) {
        return new LoggerUtil(title, message, INFO);
    }
}
