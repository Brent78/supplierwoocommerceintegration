package za.co.supplier.woocommerce.integration.Entities;

import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Optional;

@Entity
@Data
public class Profile extends AbstractEntity {

    @NotFound(action = NotFoundAction.IGNORE)
    @Length(max = 255)
    private String name;

    @NotFound(action = NotFoundAction.IGNORE)
    private String surname;

    @Email
    @NotEmpty
    @NotFound(action = NotFoundAction.IGNORE)
    @Column(nullable = false, unique = true, updatable = false)
    private String email;

    public Optional<String> getNameOpt() {
        return Optional.ofNullable(name);
    }

    public Optional<String> getSurnameOpt() {
        return Optional.ofNullable(surname);
    }

    public Optional<String> getEmailOpt() {
        return Optional.ofNullable(email);
    }
}
