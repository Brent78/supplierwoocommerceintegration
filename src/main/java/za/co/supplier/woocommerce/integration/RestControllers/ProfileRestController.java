package za.co.supplier.woocommerce.integration.RestControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.supplier.woocommerce.integration.Entities.Profile;
import za.co.supplier.woocommerce.integration.Service.ProfileService;

import javax.annotation.PostConstruct;
import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("/profile")
public class ProfileRestController extends AbstractRestController<Profile>{

    @Autowired
    private ProfileService profileService;

    @PostConstruct
    public void init() {
        setClassType(Profile.class);
    }

    @GetMapping("/get")
    public Profile profile(Principal principal) {

        Optional<String> principalName = Optional.ofNullable(principal.getName());

        if (!principalName.isPresent()) {
            return profileService.error("Please login!");
        }

        return profileService.findByEmail(principalName.get());
    }

}
