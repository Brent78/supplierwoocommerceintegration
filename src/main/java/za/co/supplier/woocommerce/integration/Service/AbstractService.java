package za.co.supplier.woocommerce.integration.Service;

import org.springframework.beans.factory.annotation.Autowired;
import za.co.supplier.woocommerce.integration.Entities.AbstractEntity;
import za.co.supplier.woocommerce.integration.Entities.Enums.LoggerUtilType;
import za.co.supplier.woocommerce.integration.Entities.LoggerUtil;
import za.co.supplier.woocommerce.integration.Repository.AbstractRepository;

import java.util.List;
import java.util.Optional;

public abstract class AbstractService<T extends AbstractEntity> {

    @Autowired
    private AbstractRepository<T> abstractRepository;

    private Class<T> classType;
    private String className;

    private static final LoggerUtilType SUCCESS = LoggerUtilType.SUCCESS;
    private static final LoggerUtilType ERROR = LoggerUtilType.ERROR;
    private static final LoggerUtilType WARNING = LoggerUtilType.WARNING;
    private static final LoggerUtilType INFO = LoggerUtilType.INFO;

    public void setClassType(Class<T> classType) {
        this.classType = classType;
        className = classType.getSimpleName().toLowerCase();
    }

    public T findById(Long id) {

        if (id != null) {

            return abstractRepository.findById(id).orElseGet(() -> error("This " + className + " does not exists."));
        } else {
            return error("Could not get " + className + ", because id is null.");
        }
    }

    public List<T> findAll() {
        return abstractRepository.findAll();
    }

    public LoggerUtil save(T entity) {

        if (entity != null) {

            if (!entity.getIdOpt().isPresent()) {
                entity.setId(0L);
            }

            if (abstractRepository.findById(entity.getId()).isPresent()) {

                return LoggerUtil.error("This " + className + " already exist, unable to save!");
            } else {

                abstractRepository.save(entity);
                return LoggerUtil.success("Your " + className + " was successfully saved.");
            }
        } else {

            return LoggerUtil.error("There was some problem trying to save " + className + ".");
        }
    }

    public LoggerUtil update(T entity) {

        if (entity != null && entity.getIdOpt().isPresent()) {

            if (abstractRepository.findById(entity.getId()).isPresent()) {

                abstractRepository.save(entity);

                return LoggerUtil.success("Your " + className + " was successfully updated.");

            } else {
                return LoggerUtil.error("This " + className + " does not exist, unable to update!");
            }
        } else {

            return LoggerUtil.error("There was some problem trying to update " + className + ".");
        }
    }

    public LoggerUtil delete(T entity) {

        if (entity != null && entity.getIdOpt().isPresent()) {

            if (abstractRepository.findById(entity.getId()).isPresent()) {

                abstractRepository.delete(entity);

                return LoggerUtil.success("Your " + className + " was successfully deleted.");

            } else {
                return LoggerUtil.error("This " + className + " does not exist.!");
            }
        } else {

            return LoggerUtil.error("There was some problem trying to delete " + className + ".");
        }
    }

    public T successOrError(T t) {

        Optional<T> entity = Optional.ofNullable(t);

        if (entity.isPresent()) {
            entity.get().setLoggerUtil(new LoggerUtil(SUCCESS.toString(), "Successfully completed proses", SUCCESS));
        } else {
            entity = Optional.of(error());
        }

        return entity.get();
    }

    public T successOrError(String message, T t) {

        Optional<T> entity = Optional.ofNullable(t);

        if (entity.isPresent()) {
            entity.get().setLoggerUtil(new LoggerUtil(SUCCESS.toString(), message, SUCCESS));
        } else {
            entity = Optional.of(error());
        }

        return entity.get();
    }

    public T successOrError(String successMsg, String errorMsg, T t) {

        Optional<T> entity = Optional.ofNullable(t);

        if (entity.isPresent()) {
            entity.get().setLoggerUtil(new LoggerUtil(SUCCESS.toString(), successMsg, SUCCESS));
        } else {
            entity = Optional.of(error(errorMsg));
        }

        return entity.get();
    }

    public T error(String message) {

        T newEntity = null;

        try {

            newEntity = classType.getDeclaredConstructor().newInstance();

            newEntity.setLoggerUtil(new LoggerUtil(ERROR.toString(), message, ERROR));

        } catch (Exception e) {
            //TODO Remember to add logger
            e.printStackTrace();
        }

        return newEntity;
    }

    public T error() {

        T newEntity = null;

        try {

            newEntity = classType.getDeclaredConstructor().newInstance();

            newEntity.setLoggerUtil(new LoggerUtil(ERROR.toString(), "There was some error with: " + classType.getSimpleName(), ERROR));

        } catch (Exception e) {
            //TODO Remember to add loggerq
            e.printStackTrace();
        }

        return newEntity;
    }
}
