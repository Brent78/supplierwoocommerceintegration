package za.co.supplier.woocommerce.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SupplierWoocommerceIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SupplierWoocommerceIntegrationApplication.class, args);
	}
}
