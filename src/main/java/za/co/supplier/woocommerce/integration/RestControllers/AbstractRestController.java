package za.co.supplier.woocommerce.integration.RestControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.supplier.woocommerce.integration.Entities.AbstractEntity;
import za.co.supplier.woocommerce.integration.Entities.LoggerUtil;
import za.co.supplier.woocommerce.integration.Service.AbstractService;

import java.util.List;

public abstract class AbstractRestController<T extends AbstractEntity> {

    @Autowired
    private AbstractService<T> abstractService;

    public void setClassType(Class<T> classType) {
        abstractService.setClassType(classType);
    }

    @GetMapping("/list")
    public List<T> list() {
        return abstractService.findAll();
    }

    @GetMapping("/get/{id}")
    public T getById(@PathVariable("id") Long id) {
        return abstractService.findById(id);
    }

    @PostMapping("/save")
    public LoggerUtil save(@RequestBody T entity) {
        return abstractService.save(entity);
    }

    @PutMapping("/update")
    public LoggerUtil update(@RequestBody T entity) {
        return abstractService.update(entity);
    }

    @DeleteMapping("/delete")
    public LoggerUtil delete(@RequestBody T entity) {
        return abstractService.delete(entity);
    }
}
