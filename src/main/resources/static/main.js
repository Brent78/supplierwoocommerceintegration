(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/Entities/AbstractEntity.ts":
/*!********************************************!*\
  !*** ./src/app/Entities/AbstractEntity.ts ***!
  \********************************************/
/*! exports provided: AbstractEntity */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AbstractEntity", function() { return AbstractEntity; });
var AbstractEntity = /** @class */ (function () {
    function AbstractEntity() {
    }
    return AbstractEntity;
}());



/***/ }),

/***/ "./src/app/Entities/Profile.ts":
/*!*************************************!*\
  !*** ./src/app/Entities/Profile.ts ***!
  \*************************************/
/*! exports provided: Profile */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Profile", function() { return Profile; });
/* harmony import */ var _AbstractEntity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AbstractEntity */ "./src/app/Entities/AbstractEntity.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var Profile = /** @class */ (function (_super) {
    __extends(Profile, _super);
    function Profile() {
        return _super.call(this) || this;
    }
    Profile.prototype.setName = function (name) {
        this.name = name;
    };
    Profile.prototype.getName = function () {
        return this.name;
    };
    Profile.prototype.setSurname = function (surname) {
        this.surname = surname;
    };
    Profile.prototype.getSurname = function () {
        return this.surname;
    };
    Profile.prototype.setEmail = function (email) {
        this.email = email;
    };
    Profile.prototype.getEmail = function () {
        return this.email;
    };
    return Profile;
}(_AbstractEntity__WEBPACK_IMPORTED_MODULE_0__["AbstractEntity"]));



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var routes = [
    { path: '', redirectTo: '', pathMatch: 'full' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\n  <mat-toolbar color=\"primary\">\n    <mat-toolbar-row>\n      <button mat-icon-button (click)=\"_toggleSidebar()\">\n        <mat-icon>menu</mat-icon>\n      </button>\n      <span class=\"navbar-spacer\"></span>\n      <h1>Supplier App</h1>\n      <span class=\"navbar-spacer\"></span>\n      <mat-icon class=\"navbar-icon\">verified_user</mat-icon>\n    </mat-toolbar-row>\n  </mat-toolbar>\n</header>\n\n<ng-sidebar-container style=\"height: 100vh;\">\n\n  <!-- A sidebar -->\n  <ng-sidebar [(opened)]=\"_opened\" mode=\"over\" closeOnClickOutside=\"true\" autoCollapseWidth=500>\n\n    <div class=\"container pl-0\">\n      <app-profile-side-bar></app-profile-side-bar>\n      <mat-accordion>\n        <mat-expansion-panel (opened)=\"panelOpenState = true\" (closed)=\"panelOpenState = false\">\n          <mat-expansion-panel-header>\n            <mat-panel-title>\n              Self aware panel\n            </mat-panel-title>\n          </mat-expansion-panel-header>\n          <p>I'm visible because I am open</p>\n        </mat-expansion-panel>\n      </mat-accordion>\n    </div>\n\n\n  </ng-sidebar>\n\n  <!-- Page content -->\n  <div ng-sidebar-content>\n    <router-outlet></router-outlet>\n  </div>\n\n</ng-sidebar-container>\n\n<!-- <router-outlet></router-outlet> -->\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar-icon {\n  padding: 0 14px; }\n\n.navbar-spacer {\n  flex: 1 1 auto; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvQzpcXFVzZXJzXFxicmVudFxcRG9jdW1lbnRzXFxpbnRlbGxpal9wcm9qZWN0c1xcc3VwcGxpZXJ3b29jb21tZXJjZWludGVncmF0aW9uXFxzcmNcXG1haW5cXGNsaWVudC9zcmNcXGFwcFxcYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0JBQWUsRUFDaEI7O0FBRUQ7RUFDRSxlQUFjLEVBQ2YiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmF2YmFyLWljb24ge1xyXG4gICAgcGFkZGluZzogMCAxNHB4O1xyXG4gIH1cclxuICBcclxuICAubmF2YmFyLXNwYWNlciB7XHJcbiAgICBmbGV4OiAxIDEgYXV0bztcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(toastr) {
        this.toastr = toastr;
        this._opened = true;
    }
    AppComponent.prototype.showSuccess = function () {
        this.toastr.success('Hello world!', 'Toastr fun!');
    };
    AppComponent.prototype._toggleSidebar = function () {
        this._opened = !this._opened;
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _material_material_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./material/material-module */ "./src/app/material/material-module.ts");
/* harmony import */ var ng_sidebar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng-sidebar */ "./node_modules/ng-sidebar/lib/index.js");
/* harmony import */ var ng_sidebar__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng_sidebar__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _profile_profile_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./profile/profile.module */ "./src/app/profile/profile.module.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _nav_bar_content_nav_bar_content_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./nav-bar-content/nav-bar-content.component */ "./src/app/nav-bar-content/nav-bar-content.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
                _nav_bar_content_nav_bar_content_component__WEBPACK_IMPORTED_MODULE_10__["NavBarContentComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"], ng_sidebar__WEBPACK_IMPORTED_MODULE_6__["SidebarModule"], _material_material_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _profile_profile_module__WEBPACK_IMPORTED_MODULE_7__["ProfileModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_8__["ToastrModule"].forRoot()
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.service.ts":
/*!********************************!*\
  !*** ./src/app/app.service.ts ***!
  \********************************/
/*! exports provided: AppService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppService", function() { return AppService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppService = /** @class */ (function () {
    function AppService(http, toastr) {
        this.http = http;
        this.toastr = toastr;
        this.toastr.toastrConfig.progressBar = true;
        this.toastr.toastrConfig.easeTime = 450;
    }
    AppService.prototype.defaultGet = function (url) {
        return this.http.get(url);
    };
    AppService.prototype.get = function (baseUrl, id) {
        return this.http.get(baseUrl + ("/get/" + id));
    };
    AppService.prototype.getList = function (baseUrl) {
        return this.http.get(baseUrl + "/list");
    };
    AppService.prototype.save = function (baseUrl, data) {
        return this.http.post(baseUrl + "/save", data);
    };
    AppService.prototype.update = function (baseUrl, data) {
        return this.http.put(baseUrl + "/update", data);
    };
    AppService.prototype.delete = function (baseUrl, id) {
        return this.http.delete(baseUrl + "/delete" + "/${id}");
    };
    AppService.prototype.toastrHttpErrorResponce = function (error) {
        console.error(error);
        this.toastr.toastrConfig.timeOut = 20000;
        this.toastr.toastrConfig.extendedTimeOut = 3000;
        this.toastr.error("Please inform admin about this error!\n HttpStatus: " + error.status, error.name);
        this.toastr.error(error.message, "Message");
    };
    AppService.prototype.toastrMsgWithOutSuccess = function (logger) {
        if (logger != null) {
            if (logger.loggerUtilType !== "SUCCESS") {
                this.toastrMsg(logger);
            }
        }
        else {
            console.error("Some problem with the toastr!");
            this.defaultErrorPopUp();
        }
    };
    AppService.prototype.toastrMsg = function (logger) {
        console.error(logger);
        if (logger != null) {
            switch (logger.loggerUtilType) {
                case "ERROR":
                    this.errorPopUp(logger);
                    break;
                case "SUCCESS":
                    this.succesPopUp(logger);
                    break;
                case "INFO":
                    this.infoPopUp(logger);
                    break;
                case "WARNING":
                    this.warningPopUp(logger);
                    break;
                default:
                    console.error("Some problem with the toastr!");
                    this.defaultErrorPopUp();
            }
        }
        else {
            console.error("Some problem with the toastr!");
            this.defaultErrorPopUp();
        }
    };
    AppService.prototype.succesPopUp = function (logger) {
        this.toastr.success(logger.message, logger.title);
    };
    AppService.prototype.errorPopUp = function (logger) {
        this.toastr.error(logger.message, logger.title);
    };
    AppService.prototype.infoPopUp = function (logger) {
        this.toastr.info(logger.message, logger.title);
    };
    AppService.prototype.warningPopUp = function (logger) {
        this.toastr.warning(logger.message, logger.title);
    };
    AppService.prototype.defaultErrorPopUp = function () {
        this.toastr.error('There was some error!', 'Default Error');
    };
    AppService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], ngx_toastr__WEBPACK_IMPORTED_MODULE_2__["ToastrService"]])
    ], AppService);
    return AppService;
}());



/***/ }),

/***/ "./src/app/material/material-module.ts":
/*!*********************************************!*\
  !*** ./src/app/material/material-module.ts ***!
  \*********************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginatorModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSortModule"]
            ],
            declarations: [],
            exports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginatorModule"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSortModule"]
            ]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/nav-bar-content/nav-bar-content.component.html":
/*!****************************************************************!*\
  !*** ./src/app/nav-bar-content/nav-bar-content.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  nav-bar-content works!\n</p>\n"

/***/ }),

/***/ "./src/app/nav-bar-content/nav-bar-content.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/nav-bar-content/nav-bar-content.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25hdi1iYXItY29udGVudC9uYXYtYmFyLWNvbnRlbnQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/nav-bar-content/nav-bar-content.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/nav-bar-content/nav-bar-content.component.ts ***!
  \**************************************************************/
/*! exports provided: NavBarContentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavBarContentComponent", function() { return NavBarContentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavBarContentComponent = /** @class */ (function () {
    function NavBarContentComponent() {
    }
    NavBarContentComponent.prototype.ngOnInit = function () {
    };
    NavBarContentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-nav-bar-content',
            template: __webpack_require__(/*! ./nav-bar-content.component.html */ "./src/app/nav-bar-content/nav-bar-content.component.html"),
            styles: [__webpack_require__(/*! ./nav-bar-content.component.scss */ "./src/app/nav-bar-content/nav-bar-content.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NavBarContentComponent);
    return NavBarContentComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/profile/profile-routing.module.ts ***!
  \***************************************************/
/*! exports provided: ProfileRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileRoutingModule", function() { return ProfileRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _profile_view_profile_view_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile-view/profile-view.component */ "./src/app/profile/profile-view/profile-view.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: 'profile/view', component: _profile_view_profile_view_component__WEBPACK_IMPORTED_MODULE_2__["ProfileViewComponent"] }
];
var ProfileRoutingModule = /** @class */ (function () {
    function ProfileRoutingModule() {
    }
    ProfileRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], ProfileRoutingModule);
    return ProfileRoutingModule;
}());



/***/ }),

/***/ "./src/app/profile/profile-side-bar/profile-side-bar.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/profile/profile-side-bar/profile-side-bar.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"card\" style=\"width: 18rem;\">\n  <div class=\"container pt-5\">\n    <div class=\"row justify-content-center align-items-center\">\n        <mat-card class=\"example-card\">\n            <img mat-card-image src=\"../assets/space.jpg\" alt=\"Photo of a Shiba Inu\">\n            <mat-card-actions>\n              <button mat-raised-button color=\"primary\" [routerLink]=\"['/profile/view']\" routerLinkActive=\"\" >Profile</button>\n            </mat-card-actions>\n          </mat-card>\n    </div>\n  </div>\n  <div class=\"card-body\">\n\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/profile/profile-side-bar/profile-side-bar.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/profile/profile-side-bar/profile-side-bar.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-card {\n  max-width: 230px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLXNpZGUtYmFyL0M6XFxVc2Vyc1xcYnJlbnRcXERvY3VtZW50c1xcaW50ZWxsaWpfcHJvamVjdHNcXHN1cHBsaWVyd29vY29tbWVyY2VpbnRlZ3JhdGlvblxcc3JjXFxtYWluXFxjbGllbnQvc3JjXFxhcHBcXHByb2ZpbGVcXHByb2ZpbGUtc2lkZS1iYXJcXHByb2ZpbGUtc2lkZS1iYXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxpQkFBZ0IsRUFDakIiLCJmaWxlIjoic3JjL2FwcC9wcm9maWxlL3Byb2ZpbGUtc2lkZS1iYXIvcHJvZmlsZS1zaWRlLWJhci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4uZXhhbXBsZS1jYXJkIHtcclxuICAgIG1heC13aWR0aDogMjMwcHg7XHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/profile/profile-side-bar/profile-side-bar.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/profile/profile-side-bar/profile-side-bar.component.ts ***!
  \************************************************************************/
/*! exports provided: ProfileSideBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileSideBarComponent", function() { return ProfileSideBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfileSideBarComponent = /** @class */ (function () {
    function ProfileSideBarComponent() {
    }
    ProfileSideBarComponent.prototype.ngOnInit = function () {
    };
    ProfileSideBarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile-side-bar',
            template: __webpack_require__(/*! ./profile-side-bar.component.html */ "./src/app/profile/profile-side-bar/profile-side-bar.component.html"),
            styles: [__webpack_require__(/*! ./profile-side-bar.component.scss */ "./src/app/profile/profile-side-bar/profile-side-bar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProfileSideBarComponent);
    return ProfileSideBarComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile-view/profile-view.component.html":
/*!******************************************************************!*\
  !*** ./src/app/profile/profile-view/profile-view.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container pt-3\">\n  <div class=\"row\">\n      <mat-card class=\"col-md-12\">\n    <div class=\"example-container\">\n        <mat-form-field >\n            <input matInput placeholder=\"*Name\" value=\"{{profile?.name}}\" [(ngModel)]=\"profile.name\">\n          </mat-form-field>\n\n          <mat-form-field>\n              <input matInput placeholder=\"*Surname\" value=\"{{profile?.surname}}\" [(ngModel)]=\"profile.surname\">\n            </mat-form-field>\n\n            <mat-form-field>\n                <input matInput placeholder=\"*Email\" value=\"{{profile?.email}}\" [(ngModel)]=\"profile.email\">\n              </mat-form-field>\n\n            \n              <div class=\"row\">\n\n                <div class=\"col-md-auto\">\n                    <button mat-raised-button color=\"primary\" (click)=\"update()\" >Save</button>\n                </div>\n              </div>\n    </div>\n    </mat-card>\n</div>\n\n<!-- <div class=\"container pt-3\">\n    <mat-card class=\"col-md-12\">\n    <div class=\"row justify-content-md-center\">\n\n      <div class=\"col-md-auto\">\n          <mat-form-field>\n              <input matInput placeholder=\"*Name\" value=\"{{profile?.name}}\">\n            </mat-form-field>\n      </div>\n    </div>\n\n    <div class=\"row justify-content-md-center\">\n\n        <div class=\"col-md-auto\">\n            <mat-form-field>\n                <input matInput placeholder=\"*Name\" value=\"{{profile?.name}}\">\n              </mat-form-field>\n        </div>\n      </div>\n\n      <div class=\"row justify-content-md-center\">\n\n          <div class=\"col-md-auto\">\n              <mat-form-field>\n                  <input matInput placeholder=\"*Name\" value=\"{{profile?.name}}\">\n                </mat-form-field>\n          </div>\n        </div>\n    </mat-card>\n  </div>\n\n  <div class=\"container pt-3\">\n      <mat-card class=\"col-md-12\">\n      <div class=\"row justify-content-md-center\">\n  \n        <div class=\"col-md-auto\">\n            <mat-form-field>\n                <input matInput placeholder=\"*Name\" value=\"{{profile?.name}}\">\n              </mat-form-field>\n        </div>\n      </div>\n  \n      <div class=\"row justify-content-md-center\">\n  \n          <div class=\"col-md-auto\">\n              <mat-form-field>\n                  <input matInput placeholder=\"*Name\" value=\"{{profile?.name}}\">\n                </mat-form-field>\n          </div>\n        </div>\n  \n        <div class=\"row justify-content-md-center\">\n  \n            <div class=\"col-md-auto\">\n                <mat-form-field>\n                    <input matInput placeholder=\"*Name\" value=\"{{profile?.name}}\">\n                  </mat-form-field>\n            </div>\n          </div>\n      </mat-card>\n    </div> -->\n\n    <mat-form-field>\n        <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\n      </mat-form-field>\n      \n      <div class=\"mat-elevation-z8\">\n        <table mat-table [dataSource]=\"dataSource\" matSort>\n      \n          <!-- ID Column -->\n          <ng-container matColumnDef=\"id\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> ID </th>\n            <td mat-cell *matCellDef=\"let row\"> {{row.id}} </td>\n          </ng-container>\n      \n          <!-- Name Column -->\n          <ng-container matColumnDef=\"name\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Name </th>\n            <td mat-cell *matCellDef=\"let row\"> {{row.name}} </td>\n          </ng-container>\n      \n          <!-- Surname Column -->\n          <ng-container matColumnDef=\"surname\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Surname </th>\n            <td mat-cell *matCellDef=\"let row\"> {{row.surname}} </td>\n          </ng-container>\n      \n          <!-- Email Column -->\n          <ng-container matColumnDef=\"email\">\n            <th mat-header-cell *matHeaderCellDef mat-sort-header> Email </th>\n            <td mat-cell *matCellDef=\"let row\" > {{row.email}} </td>\n          </ng-container>\n      \n          <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n          <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\">\n          </tr>\n        </table>\n      \n        <mat-paginator [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\n      </div>\n"

/***/ }),

/***/ "./src/app/profile/profile-view/profile-view.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/profile/profile-view/profile-view.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column; }\n\n.example-container > * {\n  width: 100%; }\n\ntable {\n  width: 100%; }\n\n.mat-form-field {\n  font-size: 14px;\n  width: 100%; }\n\ntd, th {\n  width: 25%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLXZpZXcvQzpcXFVzZXJzXFxicmVudFxcRG9jdW1lbnRzXFxpbnRlbGxpal9wcm9qZWN0c1xcc3VwcGxpZXJ3b29jb21tZXJjZWludGVncmF0aW9uXFxzcmNcXG1haW5cXGNsaWVudC9zcmNcXGFwcFxccHJvZmlsZVxccHJvZmlsZS12aWV3XFxwcm9maWxlLXZpZXcuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFhO0VBQ2IsdUJBQXNCLEVBQ3ZCOztBQUVEO0VBQ0UsWUFBVyxFQUNaOztBQUVEO0VBQ0UsWUFBVyxFQUNaOztBQUVEO0VBQ0UsZ0JBQWU7RUFDZixZQUFXLEVBQ1o7O0FBRUQ7RUFDRSxXQUFVLEVBQ1giLCJmaWxlIjoic3JjL2FwcC9wcm9maWxlL3Byb2ZpbGUtdmlldy9wcm9maWxlLXZpZXcuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG5cclxuICB0YWJsZSB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgXHJcbiAgLm1hdC1mb3JtLWZpZWxkIHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICBcclxuICB0ZCwgdGgge1xyXG4gICAgd2lkdGg6IDI1JTtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/profile/profile-view/profile-view.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/profile/profile-view/profile-view.component.ts ***!
  \****************************************************************/
/*! exports provided: ProfileViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileViewComponent", function() { return ProfileViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_Entities_Profile__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/Entities/Profile */ "./src/app/Entities/Profile.ts");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/** Constants used to fill up our data base. */
var COLORS = ['maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
    'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'];
var NAMES = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
    'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
    'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];
var ProfileViewComponent = /** @class */ (function () {
    function ProfileViewComponent(appService) {
        // // Create 100 users
        // const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));
        this.appService = appService;
        this.profile = new src_app_Entities_Profile__WEBPACK_IMPORTED_MODULE_1__["Profile"]();
        this.baseUrl = "/profile";
        this.displayedColumns = ['id', 'name', 'surname', 'email'];
        // // Assign the data to the data source for the table to render
        // this.dataSource = new MatTableDataSource(users);
    }
    ProfileViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.getList(this.baseUrl).subscribe(function (data) {
            console.log(data);
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](data);
            _this.dataSource.paginator = _this.paginator;
            _this.dataSource.sort = _this.sort;
        }, function (error) {
            _this.appService.toastrHttpErrorResponce(error);
        });
        this.appService.defaultGet(this.baseUrl + "/get").subscribe(function (data) {
            _this.profile = data;
            _this.appService.toastrMsgWithOutSuccess(data.loggerUtil);
        }, function (error) {
            _this.appService.toastrHttpErrorResponce(error);
        });
    };
    ProfileViewComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    ProfileViewComponent.prototype.testing = function () {
        console.log(this.profile);
    };
    ProfileViewComponent.prototype.update = function () {
        var _this = this;
        console.log(this.profile);
        this.profile.id = 0;
        this.appService.save(this.baseUrl, this.profile).subscribe(function (data) {
            console.log(data);
            _this.appService.toastrMsg(data);
        }, function (error) {
            console.log(error);
            _this.appService.toastrHttpErrorResponce(error);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], ProfileViewComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], ProfileViewComponent.prototype, "sort", void 0);
    ProfileViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile-view',
            template: __webpack_require__(/*! ./profile-view.component.html */ "./src/app/profile/profile-view/profile-view.component.html"),
            styles: [__webpack_require__(/*! ./profile-view.component.scss */ "./src/app/profile/profile-view/profile-view.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"]])
    ], ProfileViewComponent);
    return ProfileViewComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile.module.ts":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/*! exports provided: ProfileModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileModule", function() { return ProfileModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _profile_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile-routing.module */ "./src/app/profile/profile-routing.module.ts");
/* harmony import */ var _profile_side_bar_profile_side_bar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./profile-side-bar/profile-side-bar.component */ "./src/app/profile/profile-side-bar/profile-side-bar.component.ts");
/* harmony import */ var _material_material_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../material/material-module */ "./src/app/material/material-module.ts");
/* harmony import */ var _profile_view_profile_view_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile-view/profile-view.component */ "./src/app/profile/profile-view/profile-view.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ProfileModule = /** @class */ (function () {
    function ProfileModule() {
    }
    ProfileModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_profile_side_bar_profile_side_bar_component__WEBPACK_IMPORTED_MODULE_4__["ProfileSideBarComponent"], _profile_view_profile_view_component__WEBPACK_IMPORTED_MODULE_6__["ProfileViewComponent"]],
            exports: [_profile_side_bar_profile_side_bar_component__WEBPACK_IMPORTED_MODULE_4__["ProfileSideBarComponent"], _profile_view_profile_view_component__WEBPACK_IMPORTED_MODULE_6__["ProfileViewComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _profile_routing_module__WEBPACK_IMPORTED_MODULE_3__["ProfileRoutingModule"], _material_material_module__WEBPACK_IMPORTED_MODULE_5__["MaterialModule"]
            ]
        })
    ], ProfileModule);
    return ProfileModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\brent\Documents\intellij_projects\supplierwoocommerceintegration\src\main\client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map