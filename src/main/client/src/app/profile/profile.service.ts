import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Profile } from '../Entities/Profile';
import { Observable } from 'rxjs';
import { LoggerUtil } from '../Entities/LoggerUtil';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private profile:any;

  constructor(private http:HttpClient) { }

  public geting():Observable<Profile> {
    console.log('hello2');
    return this.http.get<Profile>('/profile/get');
    //console.log(this.profile);
  }


  public saveProfile(profile: Profile):Observable<LoggerUtil> {
    console.log("In the post " + profile);
    return this.http.post<LoggerUtil>("/profile/save", profile);
  }
}
