import { Component, OnInit, ViewChild } from '@angular/core';
import { ProfileService } from '../profile.service';
import { Profile } from 'src/app/Entities/Profile';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { LoggerUtil } from 'src/app/Entities/LoggerUtil';
import { AppService } from 'src/app/app.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

/** Constants used to fill up our data base. */
const COLORS: string[] = ['maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
  'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'];
const NAMES: string[] = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
  'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
  'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];


@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent implements OnInit {

  public profile = new Profile();
  public profileList: Profile[];
  private baseUrl: string = "/profile";

  displayedColumns: string[] = ['id', 'name', 'surname', 'email'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private appService: AppService) { 

        // // Create 100 users
        // const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

        // // Assign the data to the data source for the table to render
        // this.dataSource = new MatTableDataSource(users);
        
  }

  ngOnInit() {

    this.appService.getList(this.baseUrl).subscribe(data => {
      console.log(data);
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => {
      this.appService.toastrHttpErrorResponce(error);
    });


    this.appService.defaultGet(this.baseUrl+"/get").subscribe((data: Profile) => {
      this.profile = data;
      this.appService.toastrMsgWithOutSuccess(data.loggerUtil);
    },
      error => {
        this.appService.toastrHttpErrorResponce(error);
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public testing():void{
    console.log(this.profile);
  }

  public update() {
    console.log(this.profile);
    this.profile.id = 0;
    this.appService.save(this.baseUrl, this.profile).subscribe(data => {
      console.log(data);
      this.appService.toastrMsg(data);
    }, (error: HttpErrorResponse) => {
      console.log(error);
      this.appService.toastrHttpErrorResponce(error);
    });
  }
}
