import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileSideBarComponent } from './profile-side-bar/profile-side-bar.component';
import { MaterialModule } from '../material/material-module';
import { ProfileViewComponent } from './profile-view/profile-view.component';

@NgModule({
  declarations: [ProfileSideBarComponent, ProfileViewComponent],
  exports: [ProfileSideBarComponent, ProfileViewComponent],
  imports: [
    CommonModule, FormsModule, 
    ProfileRoutingModule, MaterialModule
  ]
})
export class ProfileModule { }
