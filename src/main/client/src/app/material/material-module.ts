import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { 
          MatToolbarModule, MatButtonModule, MatCardModule, MatIconModule,
          MatExpansionModule, MatFormFieldModule, MatInputModule, MatTableModule,
          MatPaginatorModule, MatSortModule
        } from '@angular/material';


@NgModule({
  imports: [
    CommonModule, BrowserAnimationsModule, MatToolbarModule, MatCardModule, MatIconModule,
    MatButtonModule, MatExpansionModule, MatFormFieldModule, MatInputModule, MatTableModule,
    MatPaginatorModule, MatSortModule
  ],
  declarations: [],
  exports: [
    BrowserAnimationsModule, MatToolbarModule, MatCardModule, MatIconModule,
    MatButtonModule, MatExpansionModule, MatFormFieldModule, MatInputModule, MatTableModule,
    MatPaginatorModule, MatSortModule
  ]
})
export class MaterialModule { }