import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material/material-module';
import { SidebarModule } from 'ng-sidebar';
import { ProfileModule } from './profile/profile.module';
import { ToastrModule } from 'ngx-toastr';


import { AppComponent } from './app.component';
import { NavBarContentComponent } from './nav-bar-content/nav-bar-content.component';


@NgModule({
  declarations: [
    AppComponent,
    NavBarContentComponent
  ],
  imports: [
    BrowserModule,AppRoutingModule, SidebarModule, MaterialModule, HttpClientModule,
    ProfileModule, FormsModule, ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
