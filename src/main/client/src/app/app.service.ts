import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { LoggerUtil } from './Entities/LoggerUtil';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.toastr.toastrConfig.progressBar = true;
    this.toastr.toastrConfig.easeTime = 450;
  }

  public defaultGet(url: string): Observable<any> {
    return this.http.get(url);
  }

  public get(baseUrl: string, id: number): Observable<any> {
    return this.http.get(baseUrl + `/get/${id}`);
  }

  public getList(baseUrl: string): Observable<any[]> {
    return this.http.get<any[]>(baseUrl + "/list");
  }

  public save(baseUrl: string, data: any): Observable<any> {
    return this.http.post(baseUrl + "/save", data);
  }

  public update(baseUrl: string, data: any): Observable<any> {
    return this.http.put(baseUrl + `/update`, data);
  }

  public delete(baseUrl: string, id: number): Observable<any> {
    return this.http.delete(baseUrl + "/delete" + "/${id}");
  }

  public toastrHttpErrorResponce(error: HttpErrorResponse): void {

    console.error(error);
    this.toastr.toastrConfig.timeOut = 20000;
    this.toastr.toastrConfig.extendedTimeOut = 3000;
    this.toastr.error("Please inform admin about this error!\n HttpStatus: " + error.status, error.name);
    this.toastr.error(error.message, "Message");
  }

  public toastrMsgWithOutSuccess(logger: LoggerUtil): void {

    if (logger != null) {
      if (logger.loggerUtilType !== "SUCCESS") {
        this.toastrMsg(logger);
      }
    } else {
      console.error("Some problem with the toastr!");
      this.defaultErrorPopUp();
    }
  }

  public toastrMsg(logger: LoggerUtil): void {

    console.error(logger);
    if (logger != null) {
      switch (logger.loggerUtilType) {

        case "ERROR":
          this.errorPopUp(logger);
          break;

        case "SUCCESS":
          this.succesPopUp(logger);
          break;

        case "INFO":
          this.infoPopUp(logger);
          break;

        case "WARNING":
          this.warningPopUp(logger);
          break;

        default:
          console.error("Some problem with the toastr!");
          this.defaultErrorPopUp();
      }
    } else {
      console.error("Some problem with the toastr!");
      this.defaultErrorPopUp();
    }
  }

  public succesPopUp(logger: LoggerUtil): void {
    this.toastr.success(logger.message, logger.title);
  }

  public errorPopUp(logger: LoggerUtil): void {
    this.toastr.error(logger.message, logger.title);
  }

  public infoPopUp(logger: LoggerUtil): void {
    this.toastr.info(logger.message, logger.title);
  }

  public warningPopUp(logger: LoggerUtil): void {
    this.toastr.warning(logger.message, logger.title);
  }

  public defaultErrorPopUp(): void {
    this.toastr.error('There was some error!', 'Default Error');
  }
}
