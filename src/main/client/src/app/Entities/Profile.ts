import { AbstractEntity } from "./AbstractEntity";

export class Profile extends AbstractEntity{

    private name:string;
    private surname:string;
    private email:string;

    constructor() {super()}

    public setName(name:string):void {
        this.name = name;
    }

    public getName():string {
        return this.name;
    }

    public setSurname(surname:string):void {
        this.surname = surname;
    }

    public getSurname():string {
        return this.surname;
    }

    public setEmail(email:string):void {
        this.email = email;
    }

    public getEmail():string {
        return this.email;
    }
}