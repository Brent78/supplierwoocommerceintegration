import { LoggerUtil } from "./LoggerUtil"


export class AbstractEntity {

    public id: number;

    public loggerUtil: LoggerUtil;

}